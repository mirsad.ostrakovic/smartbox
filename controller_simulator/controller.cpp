#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdexcept>
#include <thread>
#include <memory>
#include <string>

#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>

#include "../util.hpp"
#include "protocol_controller.hpp"

#define SERVER_PORT 19970

bool isDoorLocked();
bool isDoorClosed();
bool lock();
bool unlock();


bool doorClosed = true;
bool doorLocked = false;

bool isDoorLocked()
{
  return doorLocked;
}

bool isDoorClosed()
{
  return doorClosed; 
}

bool lock()
{
  doorLocked = true;
  return true;
}

bool unlock()
{
  doorLocked = false;
  return true;
}




using port_t = int;
using ip_t = std::string;

class ControllerSocket{
  public:
    ControllerSocket(ip_t ip, port_t port);
    ~ControllerSocket() { close(socketFD); }

    int getSocketFD() { return socketFD; }

  private:
    int socketFD;

};

ControllerSocket::ControllerSocket(ip_t ip, port_t port){
  
  if((socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    throw std::invalid_argument("ControllerSocket(): can not get socket");
 
  struct sockaddr_in servAddr;
  
  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(port);
  servAddr.sin_addr.s_addr = inet_addr(ip.c_str());
  
  if(connect(socketFD, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
    throw std::invalid_argument("ControllerSocket(): can not connect to server");
}






class ControllerMessage{
  public:
    static bool sendLoginMsg(int socketFD, unsigned char id[128]);
    static bool receiveLoginMsgResponse(int socketFD);
    static bool sendDoorAlreadyLockedMsg(int socketFD); 
    static bool sendDoorSuccessfullyLockedMsg(int socketFD);
    static bool sendDoorNotClosedMsg(int socketFD);
    static bool sendProblemWithLockingMsg(int socketFD);
    static bool sendDoorAlreadyUnlockedMsg(int socketFD);
    static bool sendUnlockDoorMsg(int socketFD);
    static bool sendProblemWithUnlockingMsg(int socketFD);
    static bool sendInfoMsg(int socketFD, bool isDoorLocked, bool isDoorClosed);
    static int  recvServerMessage(int socketFD);
};


bool ControllerMessage::sendLoginMsg(int socketFD, unsigned char id[128])
{
  char buffer[sizeof(ControllerToServerLoginMsg)];
  
  ControllerToServerLoginMsg* cmsg = (ControllerToServerLoginMsg *)buffer;
  
  cmsg->msgType = CMSG_LOGIN_MSG;
  memcpy(cmsg->id, id, 128);

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerLoginMsg)))
    return true;
  else
    return false;
}


bool ControllerMessage::receiveLoginMsgResponse(int socketFD)
{ 
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(socketFD, &buffer[totalLen], 1u))
    return false;

  totalLen += 1u;  
  
  ServerToControllerMsg* msg = (ServerToControllerMsg *)buffer;

  switch(msg->msgType){
    case SMSG_LOGIN_SUCCESSFULL:
      return true;
  
    case SMSG_LOGIN_ERROR:
      return false;

    default:
      return false;
  }
}


bool ControllerMessage::sendDoorAlreadyLockedMsg(int socketFD)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_DOOR_ALREADY_LOCKED;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}


bool ControllerMessage::sendDoorSuccessfullyLockedMsg(int socketFD)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_DOOR_LOCKED_SUCCESSFULLY;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}


bool ControllerMessage::sendDoorNotClosedMsg(int socketFD)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_DOOR_NOT_CLOSED;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}



bool ControllerMessage::sendProblemWithLockingMsg(int socketFD)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_ERROR_MSG;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}




bool ControllerMessage::sendDoorAlreadyUnlockedMsg(int socketFD)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_DOOR_ALREADY_UNLOCKED;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}


bool ControllerMessage::sendUnlockDoorMsg(int socketFD)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_DOOR_UNLOCKED;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}


 
bool ControllerMessage::sendProblemWithUnlockingMsg(int socketFD)
{
  char buffer[sizeof(ControllerToServerMsg)];
  
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  cmsg->msgType = CMSG_ERROR_MSG;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}

bool ControllerMessage::sendInfoMsg(int socketFD, bool isDoorLocked, bool isDoorClosed)
{ 
  char buffer[sizeof(ControllerToServerMsg)];
 
  ControllerToServerMsg* cmsg = (ControllerToServerMsg *)buffer;
  
  if(isDoorLocked)
    cmsg->msgType = isDoorClosed ? CMSG_INFO_STATUS_TL_TC : CMSG_INFO_STATUS_TL_FC;
  else
    cmsg->msgType = isDoorClosed ? CMSG_INFO_STATUS_FL_TC : CMSG_INFO_STATUS_FL_FC;

  if(sendTo(socketFD, buffer, sizeof(ControllerToServerMsg)))
    return true;
  else
    return false;
}






#define ERROR_IN_READING_RESPONSE -0x1
#define ERROR_INVALID_SERVER_MSG -0x2

//#define SMSG_LOCK_MSG 0x1
//#define SMSG_UNLOCK_MSG 0x2
//#define SMSG_UNLOCK_MSG_ACK 0x3
//#define SMSG_INFO_MSG 0x4
//#define SMSG_LOGIN_SUCCESSFULL 0x5
//#define SMSG_LOGIN_ERROR 0x6

int ControllerMessage::recvServerMessage(int socketFD)
{  
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(socketFD, &buffer[totalLen], 1u))
    return ERROR_IN_READING_RESPONSE;

  totalLen += 1u;  
  
  ServerToControllerMsg* msg = (ServerToControllerMsg *)buffer;

  switch(msg->msgType)
  {
    case SMSG_LOCK_MSG:
      return SMSG_LOCK_MSG;

    case SMSG_UNLOCK_MSG:
      return SMSG_UNLOCK_MSG;

    case SMSG_INFO_MSG:
      return SMSG_INFO_MSG;

    default:
      return ERROR_IN_READING_RESPONSE;
  }
}







int main(int argc, char *argv[])
{ 
  ControllerSocket controllerSocket("127.0.0.1", SERVER_PORT);
  unsigned char id[128] = "smartBox1234";

  if(!ControllerMessage::sendLoginMsg(controllerSocket.getSocketFD(), id))
  {
    std::cerr << "main(): sendLoginMsg() error" << std::endl;
    return -1;
  }

  if(!ControllerMessage::receiveLoginMsgResponse(controllerSocket.getSocketFD()))
  {
    std::cerr << "main(): receiveLoginMsgResponse() error" << std::endl;
    return -1;
  }


  
  while(true)
  {
    int serverMsg = ControllerMessage::recvServerMessage(controllerSocket.getSocketFD());

    switch(serverMsg)
    {
      case SMSG_LOCK_MSG:
      {
        if(isDoorLocked())
        {
          ControllerMessage::sendDoorAlreadyLockedMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage::sendDoorAlreadyLockedMsg()" << std::endl;
          break;
        }
      
        if(!isDoorClosed())
        {
          ControllerMessage::sendDoorNotClosedMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage::sendDoorNotClosedMsg()" << std::endl;
          break;
        }
          
        if(lock())
        {
          ControllerMessage::sendDoorSuccessfullyLockedMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage::sendDoorSuccessfullyLockedMsg()" << std::endl;
        }
        else
        {
          ControllerMessage::sendProblemWithLockingMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage::sendProblemWithLockingMsg()" << std::endl;
        }
      }
      break;

      case SMSG_UNLOCK_MSG:
      {
        if(!isDoorLocked())
        {
          ControllerMessage::sendDoorAlreadyUnlockedMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage()::sendDoorAlreadyUnlockedMsg()" << std::endl;
          break;
        }

        if(unlock())
        {
          ControllerMessage::sendUnlockDoorMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage()::sendUnlockDoorMsg()" << std::endl;
        }
        else
        {
          ControllerMessage::sendProblemWithUnlockingMsg(controllerSocket.getSocketFD());
          std::cerr << "ControllerMessage()::sendProblemWithUnlockingMsg()" << std::endl;
        }   
      }
      break;

      case SMSG_INFO_MSG:
      {
        ControllerMessage::sendInfoMsg(controllerSocket.getSocketFD(), isDoorLocked(), isDoorClosed());
        std::cerr << "ContorllerMessage()::sendInfoMsg()" << std::cerr;
      }
      break;

      default:
        std::cerr << "main(): invalid server response" << std::endl;
    }
  }



  return 0;
}
