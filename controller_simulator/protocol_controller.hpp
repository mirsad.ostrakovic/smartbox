#ifndef PROTOCOL_CONTROLLER_HPP
#define PROTOCOL_CONTROLLER_HPP




// CONTROLLER -> SERVER
#define CMSG_LOGIN_MSG 0x0

struct ControllerToServerLoginMsg{
  unsigned char msgType;
  unsigned char id[128]; // username + password od 64 bajta
};


// SERVER -> CONTROLLER 
#define SMSG_LOCK_MSG 0x1
#define SMSG_UNLOCK_MSG 0x2
#define SMSG_INFO_MSG 0x3
#define SMSG_LOGIN_SUCCESSFULL 0x4
#define SMSG_LOGIN_ERROR 0x5


struct ServerToControllerMsg{
  unsigned char msgType;
};

// CONTROLLER -> SERVER

// MSG: LOCK_MSG
// a) DOOR_ALREADY_LOCKED 
// b) DOOR_LOCKED_SUCCESSFULLY
// c) DOOR_NOT_CLOSED
// d) ERROR_MSG

// a) vrata su vec zakljucana i kontroler o tome obavjestava server
// b) vrata cu uspjesno zakljucana i kontroler o tome obavjestava server
// c) vrata nisu zatvorena
// d) greska pri zakljucavanju

#define CMSG_DOOR_ALREADY_LOCKED 0x1
#define CMSG_DOOR_LOCKED_SUCCESSFULLY 0x2
#define CMSG_DOOR_NOT_CLOSED 0x3
#define CMSG_ERROR_MSG 0x4


// CONTROLLER -> SERVER

// MSG: UNLOCK_MSG
// a) DOOR_ALREADY_UNLOCKED
// b) DOOR_UNLOCKED
// c) ERROR_MSG

// a) vrata su vec otkljucana
// b) vrata su otkljucana
// c) desila se greska priliko otkljucavanja vrata

#define CMSG_DOOR_ALREADY_UNLOCKED 0x5
#define CMSG_DOOR_UNLOCKED 0x6

// CONTROLLER -> SERVER
// MSG: INFO_MSG
// a) INFO_STATUS_TL_TC
// b) INFO_STATUS_TL_FC
// c) INFO_STATUS_FL_TC
// d) INFO_STATUS FL_FC
//
// a,b,c,d) INFO_STATUS kombinacije su T - true   F - false   L - locked  C - closed
// locked as DOOR_LOCKED
// closed as DOOR_CLOSED


#define CMSG_INFO_STATUS_TL_TC 0x7
#define CMSG_INFO_STATUS_TL_FC 0x8
#define CMSG_INFO_STATUS_FL_TC 0x9
#define CMSG_INFO_STATUS_FL_FC 0xa

struct ControllerToServerMsg{
  unsigned char msgType;
};


#endif
