#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdexcept>
#include <thread>
#include <vector>

#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>

#include "../user_message.hpp"
#include "../util.hpp"
#include "../controller_simulator/protocol_controller.hpp"
#include "server_database.hpp"

#define SERVER_PORT_USER 15030
#define SERVER_PORT_CONTROLLER 19970

using port_t = int;


void print(const char msg[], std::size_t msgLen){
  for( auto i = 0u ; i < msgLen ; ++i )
    std::cout << (int)msg[i];
}

class ServerSocket{
  public:
    ServerSocket(port_t port);
    
    int getClient(struct sockaddr_in* addr);

  private:
    int socketFD;

};

void clientService(int clientSock);

ServerSocket::ServerSocket(port_t port){
  
  if((socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    throw std::invalid_argument("ServerSocket(): can not get socket");
 
  struct sockaddr_in servAddr;
  
  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(port);
  servAddr.sin_addr.s_addr = INADDR_ANY;
  
  if(bind(socketFD, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
    throw std::invalid_argument("ServerSocket(): can not bind to port");

  if(listen(socketFD, 20) < 0)
    throw std::invalid_argument("ServerSocket(): can not listen");

}

int ServerSocket::getClient(struct sockaddr_in* addr){
  socklen_t addrLen = sizeof(struct sockaddr_in);
  return accept(socketFD, (struct sockaddr *)addr, addr != nullptr ? &addrLen : nullptr);
}



class ServerMessage{
  public:
    static bool sendInvalidMsg(int socketFD);
    static bool sendInvalidUsername(int socketFD);
    static bool sendInvalidPassword(int socketFD);
    static bool sendBoxesList(int socketFD);
    static bool sendBoxIsAllocated(int socketFD, BoxPublicID boxPublicID);
    static bool sendBoxIsLocked(int socketFD, BoxPublicID boxPublicID);
    static bool sendInvalidBoxPublicID(int socketFD);
    static bool sendBoxAlreadyReserved(int socketFD);
    static bool sendBoxSuccessfullyAllocated(int socketFD, AllocationTime allocationTime);
    static bool sendBoxDoorIsNotClosed(int socketFD);
    static bool sendBoxIsNotAllocated(int socketFD);
    static bool sendBoxSuccessfullyLocked(int socketFD, AllocationTime allocationTime, BoxPrivateKey privateKey);
    static bool sendBoxIsUnlocked(int socketFD);
    static bool sendBoxIsNotLocked(int socketFD);
    static bool sendBoxPrivateKeyInvalid(int socketFD);
};



bool ServerMessage::sendBoxIsUnlocked(int socketFD)
{ 
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_BOX_IS_UNLOCKED;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendBoxIsNotLocked(int socketFD)
{
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_BOX_NOT_LOCKED;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendBoxPrivateKeyInvalid(int socketFD)
{ 
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_BOX_PRIVATEKEY_IS_INVALID;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendBoxIsNotAllocated(int socketFD){
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_BOX_IS_NOT_ALLOCATED;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendBoxSuccessfullyLocked(int socketFD, AllocationTime allocationTime, BoxPrivateKey privateKey){
  char buffer[sizeof(ServerMessageBoxLocked)];
  
  ServerMessageBoxLocked* smsg = (ServerMessageBoxLocked *)buffer;
  smsg->srvMsgBase.type = SMSG_BOX_SUCCESSFULLY_LOCKED;
  memcpy(&smsg->timestamp, &allocationTime.timestamp, sizeof(AllocationTime::timestamp));
  memcpy(&smsg->allocationPeriod, &allocationTime.allocationPeriod, sizeof(AllocationTime::allocationPeriod));
  memcpy(&smsg->privateKey.privateKey, &privateKey.privateKey, sizeof(BoxPrivateKey::privateKey));
  
  if(sendTo(socketFD, buffer, sizeof(ServerMessageBoxLocked)))
    return true;
  else
    return false;
}


bool ServerMessage::sendBoxDoorIsNotClosed(int socketFD){    
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_BOX_DOOR_IS_NOT_CLOSED;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendBoxSuccessfullyAllocated(int socketFD, AllocationTime allocationTime){
  char buffer[sizeof(ServerMessageBoxAllocated)];
  
  ServerMessageBoxAllocated* smsg = (ServerMessageBoxAllocated *)buffer;
  smsg->srvMsgBase.type = SMSG_BOX_SUCCESSFULLY_ALLOCATED;
  memcpy(&smsg->timestamp, &allocationTime.timestamp, sizeof(AllocationTime::timestamp));
  memcpy(&smsg->allocationPeriod, &allocationTime.allocationPeriod, sizeof(AllocationTime::allocationPeriod));
  
  if(sendTo(socketFD, buffer, sizeof(ServerMessageBoxIsAllocated)))
    return true;
  else
    return false;
}


bool ServerMessage::sendBoxAlreadyReserved(int socketFD){
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_BOX_ALREADY_RESERVED;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendInvalidBoxPublicID(int socketFD){
  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_INVALID_PUBLIC_ID_OF_BOX;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendBoxIsAllocated(int socketFD, BoxPublicID boxPublicID){
  char buffer[sizeof(ServerMessageBoxIsAllocated)];
  
  ServerMessageBoxIsAllocated* smsg = (ServerMessageBoxIsAllocated *)buffer;
  smsg->srvMsgBase.type = SMSG_BOX_IS_ALLOCATED;
  memcpy(smsg->publicID.id, boxPublicID.id, sizeof(BoxPublicID));
  
  if(sendTo(socketFD, buffer, sizeof(ServerMessageBoxIsAllocated)))
    return true;
  else
    return false;
}


bool ServerMessage::sendBoxIsLocked(int socketFD, BoxPublicID boxPublicID){
  char buffer[sizeof(ServerMessageBoxIsAllocated)];
  
  ServerMessageBoxIsAllocated* smsg = (ServerMessageBoxIsAllocated *)buffer;
  smsg->srvMsgBase.type = SMSG_BOX_IS_LOCKED;
  memcpy(smsg->publicID.id, boxPublicID.id, sizeof(BoxPublicID));
  
  if(sendTo(socketFD, buffer, sizeof(ServerMessageBoxIsAllocated)))
    return true;
  else
    return false;
}



bool ServerMessage::sendBoxesList(int socketFD){
  
  char buffer[256*sizeof(BoxPublicID) + sizeof(ServerMessageFreeList::srvMsgBase) + sizeof(ServerMessageFreeList::length)];
  std::vector<BoxPublicID> freeBoxList = getFreeBoxList();

  ServerMessageFreeList* smsg = (ServerMessageFreeList *)buffer;
  smsg->srvMsgBase.type = SMSG_BOXES_LIST;
  smsg->length = freeBoxList.size() > 255u ? 255u : freeBoxList.size();
  
  BoxPublicID* boxList = (BoxPublicID*)&buffer[sizeof(smsg->srvMsgBase) + sizeof(smsg->length)];
  for(auto i = 0u ; i < smsg->length ; ++i)
    memcpy(&boxList[i], freeBoxList.at(i).id, sizeof(BoxPublicID));

  if(sendTo(socketFD, buffer, sizeof(smsg->srvMsgBase) + sizeof(smsg->length) + sizeof(BoxPublicID)*smsg->length))
    return true;
  else
    return false;

}


bool ServerMessage::sendInvalidPassword(int socketFD){

  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_INVALID_PASSWORD;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendInvalidUsername(int socketFD){

  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_INVALID_USERNAME;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}


bool ServerMessage::sendInvalidMsg(int socketFD){

  char buffer[8];

  ServerMessageBase* smsg = (ServerMessageBase *)buffer;
  smsg->type = SMSG_INVALID_MSG;

  if(send(socketFD, buffer, sizeof(ServerMessageBase), 0) <= 0)
    return false;
  else
    return true;
}



void clientService(int clientSock){

  SocketGuard socketGuard(clientSock);
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(clientSock, buffer, sizeof(UserMessageBase))){
    std::cerr << "clientService(): can not read from socket msg type, username and password" << std::endl;
    return;
  }
  
  totalLen += sizeof(UserMessageBase);

  UserMessageBase* umsg = (UserMessageBase *)buffer;

  std::cerr << "USERNAME: "; print(umsg->username, 64); std::cout << std::endl;
  std::cerr << "PASSWORD: "; print(umsg->password, 64); std::cout << std::endl;

  if(!checkUsername(umsg->username)){
    if(!ServerMessage::sendInvalidUsername(clientSock))
      std::cerr << "clientService(): ServerMessage::sendInvalidUsername(clientSock) can not be sent" << std::endl;
      return;
  }

  if(!checkPassword(umsg->username, umsg->password)){
    if(!ServerMessage::sendInvalidPassword(clientSock))
      std::cerr << "clientService(): ServerMessage::sendInvalidPassword(clientSock) can not be sent" << std::endl;
      return;
  }



  switch(buffer[0]){
   
    case UMSG_LIST_FREE:
    {  
      auto userBoxStatus = getUserBoxStatus(umsg->username);
      
      switch(userBoxStatus.first){
        case UserBoxStatus::HAVE_NOT:
        { 
          std::cout << "UMSG_LIST_FREE" << std::endl;
        
          if(!ServerMessage::sendBoxesList(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxesList(clientSock) can not be send" << std::endl;
          else
            std::cerr << "ServerMessage::sendBoxesList(): sent successfully" << std::endl;
          return;
        }
        break;

        case UserBoxStatus::ALLOCATED:
        {
          std::cout << "SMSG_BOX_IS_ALLOCATED" << std::endl;

          if(!ServerMessage::sendBoxIsAllocated(clientSock, userBoxStatus.second))
            std::cerr << "clientService(): ServerMessage::sendBoxIsAllocated(clientSock, userBox) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxIsAllocated(clientSock, userBox) sent successfully" << std::endl;
          return;
        }
        break;

        case UserBoxStatus::LOCKED:
        {
          std::cout << "SMSG_BOX_IS_LOCKED" << std::endl;

          if(!ServerMessage::sendBoxIsLocked(clientSock, userBoxStatus.second))
            std::cerr << "clientService(): ServerMessage::sendBoxIsLocked(clientSock, userBox) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxIsLocked(clientSock, userBox) sent successfully" << std::endl;
          return;
        }
         break;
      }
    }
    break;

    case UMSG_ALLOCATE_BOX:
    {

      if(!recvFrom(clientSock, &buffer[totalLen], sizeof(UserMessageAllocateBox) - sizeof(UserMessageBase))){
        std::cerr << "clientService(): can not read from socket UserMessageAllocateBox" << std::endl;
        return;
      }
  
      UserMessageAllocateBox* umsg = (UserMessageAllocateBox *)buffer; 
      auto userBoxStatus = allocateBox(umsg->usrMsgBase.username, umsg->publicID);
      
      switch(userBoxStatus.first){
        case BoxAllocateStatus::INVALID_PUBLIC_ID:
        {  
          if(!ServerMessage::sendInvalidBoxPublicID(clientSock))
            std::cerr << "clientService(): ServerMessage::sendInvalidBoxPublicID(clientSock) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendInvalidBoxPublicID(clientSock) sent successfully" << std::endl;
          return;
        }

        case BoxAllocateStatus::BOX_ALREADY_ALLOCATED:
        case BoxAllocateStatus::BOX_ALREADY_LOCKED:
        {
          if(!ServerMessage::sendBoxAlreadyReserved(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxAlreadyReserved(clientSock) cen not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxAlreadyReserved(clientSock) sent successfully" << std::endl;
          return;
        }

        case BoxAllocateStatus::SUCCESSFULLY_ALLOCATED:
        {
          if(!ServerMessage::sendBoxSuccessfullyAllocated(clientSock, userBoxStatus.second))
            std::cerr << "clientService(): ServerMessage::sendBoxSuccessfullyAllocated(clientSock, allocationTime) cen not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxSuccessfullyAllocated(clientSock, allocationTime) sent successfully" << std::endl;
          return;
          
        }

      }  
    }

    case UMSG_LOCK_BOX:
    {

      if(!recvFrom(clientSock, &buffer[totalLen], sizeof(UserMessageLockBox) - sizeof(UserMessageBase))){
        std::cerr << "clientService(): can not read from socket UserMessageLockBox" << std::endl;
        return;
      }
  
      UserMessageLockBox* umsg = (UserMessageLockBox *)buffer; 
      auto userBoxStatus = lockBox(umsg->usrMsgBase.username, umsg->publicID);
      std::cout << "HEHE" << std::endl;
      switch(std::get<0>(userBoxStatus)){
        
        case BoxLockStatus::INVALID_PUBLIC_ID:
        {  
          if(!ServerMessage::sendInvalidBoxPublicID(clientSock))
            std::cerr << "clientService(): ServerMessage::sendInvalidBoxPublicID(clientSock) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendInvalidBoxPublicID(clientSock) sent successfully" << std::endl;
          return;
        }

        case BoxLockStatus::BOX_ALREADY_ALLOCATED:
        case BoxLockStatus::BOX_ALREADY_LOCKED:
        {
          if(!ServerMessage::sendBoxAlreadyReserved(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxAlreadyReserved(clientSock) cen not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxAlreadyReserved(clientSock) sent successfully" << std::endl;
          return;
        }

        case BoxLockStatus::SUCCESSFULLY_LOCKED:
        {
          std::cout << "HEHE2" << std::endl;
          if(!ServerMessage::sendBoxSuccessfullyLocked(clientSock, std::get<1>(userBoxStatus), std::get<2>(userBoxStatus)))
            std::cerr << "clientService(): ServerMessage::sendBoxSuccessfullyLocked(clientSock, allocationTime, privateKey) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxSuccessfullyLocked(clientSock, allocationTime, privateKey) sent successfully" << std::endl;
          return;
          
        }

        case BoxLockStatus::BOX_DOOR_IS_NOT_CLOSED:
        { 
          if(!ServerMessage::sendBoxDoorIsNotClosed(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxDoorIsNotClosed(clientSock) cen not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxDoorIsNotClosed(clientSock) sent successfully" << std::endl;
          return;
        }

        case BoxLockStatus::BOX_NOT_ALLOCATED:
        {
          if(!ServerMessage::sendBoxIsNotAllocated(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxIsNotAllocated(clientSock) cen not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxIsNotAllocated(clientSock) sent successfully" << std::endl;
          return;
        }

      }  
    
    }

    case UMSG_UNLOCK_BOX:
    {
      
      if(!recvFrom(clientSock, &buffer[totalLen], sizeof(UserMessageUnlockBox) - sizeof(UserMessageBase))){
        std::cerr << "clientService(): can not read from socket UserMessageUnlockBox" << std::endl;
        return;
      }
  
      UserMessageUnlockBox* umsg = (UserMessageUnlockBox *)buffer; 
      auto userBoxStatus = unlockBox(umsg->usrMsgBase.username, umsg->publicID, umsg->boxPrivateKey);
      switch(userBoxStatus){
        
        case BoxUnlockStatus::INVALID_PUBLIC_ID:
        {  
          if(!ServerMessage::sendInvalidBoxPublicID(clientSock))
            std::cerr << "clientService(): ServerMessage::sendInvalidBoxPublicID(clientSock) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendInvalidBoxPublicID(clientSock) sent successfully" << std::endl;
          return;
        }


        case BoxUnlockStatus::BOX_NOT_LOCKED:
        {  
          if(!ServerMessage::sendBoxIsNotLocked(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxIsLocked(clientSock) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxIsLocked(clientSock) sent successfully" << std::endl;
          return;
        }
      

        case BoxUnlockStatus::BOX_INVALID_PUBLICKEY:
        {  
          if(!ServerMessage::sendBoxPrivateKeyInvalid(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxPrivateKeyInvalid(clientSock) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxPrivateKeyInvalid(clientSock) sent successfully" << std::endl;
          return;
        }
      

        case BoxUnlockStatus::SUCCESSFULLY_UNLOCKED:
        {  
          if(!ServerMessage::sendBoxIsUnlocked(clientSock))
            std::cerr << "clientService(): ServerMessage::sendBoxIsUnlocked(clientSock) can not be send" << std::endl;
          else
            std::cerr << "clientService(): ServerMessage::sendBoxIsUnlocked(clientSock) sent successfully" << std::endl;
          return;
        }
      
      }

    }

    default:
      if(!ServerMessage::sendInvalidMsg(clientSock)){
        std::cerr << "clientService(): ServerMessage::sendInvalidMsg(clientSock) can not be sent" << std::endl;
        return;
      } 
  }
}


void controllerService(int clientSock)
{

  //SocketGuard socketGuard(clientSock);
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(clientSock, buffer, sizeof(ControllerToServerLoginMsg))){
    std::cerr << "controllerService(): can not read from socket controller LOGIN message" << std::endl;
    return;
  }
  
  totalLen += sizeof(ControllerToServerLoginMsg);

  ControllerToServerLoginMsg* cmsg = (ControllerToServerLoginMsg *)buffer;

  std::cerr << "ID: "; print(cmsg->id, 128); std::cout << std::endl;

  if(!checkControllerID(cmsg->id)){
   // if(!ServerMessage::sendInvalidUsername(clientSock))
   //   std::cerr << "clientService(): ServerMessage::sendInvalidUsername(clientSock) can not be sent" << std::endl;
    return;
  }
  
  addControllerSocket(cmsg->id, clientSock);
}



void userThread()
{
  ServerSocket servSockUser(SERVER_PORT_USER);
  int userSock;

  while(true){
    userSock = servSockUser.getClient(nullptr);
    std::thread(clientService, userSock).detach();
  }
}

void controllerThread()
{ 
  ServerSocket servSockController(SERVER_PORT_CONTROLLER);
  int controllerSock;

  while(true){
    controllerSock = servSockController.getClient(nullptr);
    std::thread(controllerService, controllerSock).detach();
  }
}

int main(int argc, char *argv[]){
 
  std::thread(userThread).detach();
  std::thread(controllerThread).detach();

  //ServerSocket servSockController(SERVER_PORT_CONTROLLER);
  //int userSock, controllerSock;

  //while(true){
  //  userSock = servSockUser.getClient(nullptr);
  //  std::thread(clientService, userSock).detach();
  //}
  return 0;
}
