
struct BoxPublicID{
  char id[8];
};


#define UMSG_LIST_FREE 0x1

struct UserMessageBase{
  char type;
  char username[64];
  char password[64];
};


#define SMSG_INVALID_MSG 0x1
#define SMSG_INVALID_USERNAME 0x2
#define SMSG_INVALID_PASSWORD 0x3
#define SMSG_BOXES_LIST 0x4
#define SMSG_BOX_IS_ALLOCATED 0x5
#define SMSG_BOX_IS_LOCKED 0x6

struct ServerMessageBase{
  char type; // <= SMSG_INVALID_MSG || SMSG_INVALID_USERNAME || SMSG_INVALID_PASSWORD
};

struct SeverMessageFreeList{
  ServerMessageBase srvMsgBase; // <= SMSG_BOXES_LIST
  unsigned char length;
  BoxPublicID* boxList;
};

struct ServerMessageBoxIsAllocated{
  ServerMessageBase srvMsgBase; // <= SMGS_BOX_IS_ALLOCATED
  BoxPublicID publicID;
};

struct ServerMessageBoxIsLocked{
  ServerMessageBase srvMsgBase; // <= SMG_BOX_IS_LOCKED
  BoxPublicID publicID;
};






