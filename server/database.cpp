#include "database.hpp"


bool Database::executeQuery(const std::string& query,
                           int (*callback)(void*,int,char**,char**),
                           void *callbackArg)
{
  int queryStatus;
  char *errorMsg;
  
  if( (queryStatus = sqlite3_exec(database, query.c_str(), callback, callbackArg, &errorMsg)) != SQLITE_OK )
  {
    std::cerr << "Databas::executeQuery(): error " << errorMsg << std::endl;
    sqlite3_free(errorMsg);
    return false;
  }
  return true;
}


bool Database::executeQuery(const char *query,
                           int (*callback)(void*,int,char**,char**),
                           void *callbackArg)
{
  int queryStatus;
  char *errorMsg;
  
  if( (queryStatus = sqlite3_exec(database, query, callback, callbackArg, &errorMsg)) != SQLITE_OK )
  {
    std::cerr << "Databas::executeQuery(): error " << errorMsg << std::endl;
    sqlite3_free(errorMsg);
    return false;
  }
  return true;
}


Database::Database(const std::string& databaseName)
{
  if(sqlite3_open(databaseName.c_str(), &database))
  {
    sqlite3_close(database);
    throw std::invalid_argument("Database(cons std::string&): error in opening database");
  }
}

Database::~Database()
{
  sqlite3_close(database); 
}

