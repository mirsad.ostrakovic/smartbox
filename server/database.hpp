#ifndef DATABASE_HPP
#define DATABASE_HPP

#include <string>
#include <stdexcept>
#include <iostream>

#include "sqlite3.h"

class Database{
  public:
    Database(const std::string& databaseName);
    
    bool executeQuery(const std::string& query,
                      int (*callback)(void*,int,char**,char**),
                      void *callbackArg);

    bool executeQuery(const char *query,
                      int (*callback)(void*,int,char**,char**),
                      void *callbackArg);
    
    ~Database();

  private:
    sqlite3 *database;
};


#endif
