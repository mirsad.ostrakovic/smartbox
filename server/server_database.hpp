#ifndef SERVER_DATABASE_HPP
#define SERVER_DATABASE_HPP

#include <vector>
#include <cstring>
#include <tuple>
#include <cstring>
#include <iostream>


#include "../user_message.hpp"
#include "database.hpp"


const char usernameQuery[] = "SELECT * FROM users WHERE users.USERNAME = \"%s\"";
const char passwordQuery[] = "SELECT * FROM users WHERE users.USERNAME = \"%s\" AND users.PASSWORD = \"%s\"";


Database database("smartbox.db");

enum class UserBoxStatus : unsigned char{
  
  HAVE_NOT,
  ALLOCATED,
  LOCKED
};

enum class BoxAllocateStatus : unsigned char{
  INVALID_PUBLIC_ID,
  BOX_ALREADY_ALLOCATED,
  BOX_ALREADY_LOCKED,
  SUCCESSFULLY_ALLOCATED
};

enum class BoxLockStatus : unsigned char{ 
  INVALID_PUBLIC_ID,
  BOX_ALREADY_ALLOCATED,
  BOX_ALREADY_LOCKED,
  BOX_DOOR_IS_NOT_CLOSED,
  BOX_NOT_ALLOCATED,
  SUCCESSFULLY_LOCKED
};


enum class BoxUnlockStatus : unsigned char{ 
  INVALID_PUBLIC_ID,
  BOX_NOT_LOCKED,
  BOX_INVALID_PUBLICKEY,
  SUCCESSFULLY_UNLOCKED
};

struct AllocationTime{
  unsigned char allocationPeriod;
  unsigned int timestamp;
};



bool checkControllerID(char id[128]);
bool checkUsername(char username[64]);
bool checkPassword(char username[64], char password[64]);


void addControllerSocket(char id[128], int clientSock);


std::vector<BoxPublicID> getFreeBoxList();
std::pair<UserBoxStatus, BoxPublicID> getUserBoxStatus(char username[64]);
std::pair<BoxAllocateStatus, AllocationTime> allocateBox(char username[64], BoxPublicID boxPublicID);
std::tuple<BoxLockStatus, AllocationTime, BoxPrivateKey> lockBox(char username[64], BoxPublicID boxPublicID);
BoxUnlockStatus unlockBox(char username[64], BoxPublicID boxPublicID, BoxPrivateKey privateKey);



BoxUnlockStatus unlockBox(char username[64], BoxPublicID boxPublicID, BoxPrivateKey privateKey){
  return BoxUnlockStatus::SUCCESSFULLY_UNLOCKED;
}


std::pair<BoxAllocateStatus, AllocationTime> allocateBox(char username[64], BoxPublicID boxPublicID){
  return {BoxAllocateStatus::SUCCESSFULLY_ALLOCATED, AllocationTime()};
}




std::tuple<BoxLockStatus, AllocationTime, BoxPrivateKey> lockBox(char username[64], BoxPublicID boxPublicID){
  BoxPrivateKey privateKey;
  privateKey.privateKey[0] = 1;
  return std::make_tuple(BoxLockStatus::SUCCESSFULLY_LOCKED, AllocationTime(), privateKey);

}


bool checkBoxPublicID(BoxPublicID boxPublicID){
  return true;
}


std::pair<UserBoxStatus, BoxPublicID> getUserBoxStatus(char username[64]){
  BoxPublicID publicID;
  memcpy(publicID.id, "beeffeed", 8);
  return {UserBoxStatus::HAVE_NOT, publicID};
}


std::vector<BoxPublicID> getFreeBoxList(){
  std::vector<BoxPublicID> freeBoxList;
  BoxPublicID boxPublicID;
  
  memcpy(boxPublicID.id, "abcd1234", 8);
  freeBoxList.push_back(boxPublicID);

  memcpy(boxPublicID.id, "12345678", 8);
  freeBoxList.push_back(boxPublicID);

  return freeBoxList;
}


static int checkUsernameCallback(void *isExists, int argc, char **argv, char **azColName)
{
  *((bool *)isExists) = true;
  return 0;
}


bool checkUsername(char username[64])
{
  char query[256];
  bool isExists = false;
  bool queryStatus;

  sprintf(query, usernameQuery, username);
  std::cerr << "checkUsername(): query ->" << query << "<-" << std::endl;

  queryStatus = database.executeQuery(query, checkUsernameCallback, &isExists);

  return queryStatus && isExists;
}



static int checkPasswordCallback(void *isExists, int argc, char **argv, char **azColName)
{
  *((bool *)isExists) = true;
  return 0;
}

bool checkPassword(char username[64], char password[64])
{
  char query[256];
  bool isExists = false;
  bool queryStatus;

  sprintf(query, passwordQuery, username, password);
  std::cerr << "checkPassword(): query ->" << query << "<-" << std::endl;

  queryStatus = database.executeQuery(query, checkPasswordCallback, &isExists);

  return queryStatus && isExists;
}




#endif
