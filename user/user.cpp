#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdexcept>
#include <thread>
#include <memory>
#include <string>

#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>

#include "../user_message.hpp"
#include "../util.hpp"

#define SERVER_PORT 15030

using port_t = int;
using ip_t = std::string;


void print(const char* msg, std::size_t msgLen){
  for( auto i = 0u ; i < msgLen ; ++i )
    std::cout << (int)msg[i];
}

void printAsChar(const char* msg, std::size_t msgLen){
  for( auto i = 0u ; i < msgLen ; ++i )
    std::cout << (char)msg[i];
}



struct Message{
  
  Message(std::size_t buffLen):
    buffer{ new char[buffLen] },
    bufferLen{ buffLen }
  {}

  ~Message() { delete []((char *)buffer); }

  void* buffer;
  std::size_t bufferLen;
};



enum class ListMsgResponseStatus : unsigned char{
  INVALID_USER_MSG = 0,
  INVALID_USERNAME,
  INVALID_PASSWORD,
  BOXES_LIST,
  BOX_IS_LOCKED,
  BOX_IS_ALLOCATED,
  INVALID_SERVER_RESPONSE
};

enum class AllocateMsgResponseStatus : unsigned char{
  INVALID_USER_MSG = 0,
  INVALID_USERNAME,
  INVALID_PASSWORD,
  INVALID_PUBLIC_ID_OF_BOX,
  BOX_ALREADY_RESERVED,
  BOX_SUCCESSFULLY_ALLOCATED,
  INVALID_SERVER_RESPONSE
};

enum class LockMsgResponseStatus : unsigned char{
  INVALID_USER_MSG = 0,
  INVALID_USERNAME,
  INVALID_PASSWORD,
  INVALID_PUBLIC_ID_OF_BOX,
  BOX_ALREADY_RESERVED,
  BOX_NOT_ALLOCATED,
  BOX_SUCCESSFULLY_LOCKED,
  BOX_DOOR_IS_NOT_CLOSED,
  INVALID_SERVER_RESPONSE
};

enum class UnlockMsgResponseStatus : unsigned char{
  INVALID_USER_MSG = 0,
  INVALID_USERNAME,
  INVALID_PASSWORD,
  INVALID_PUBLIC_ID_OF_BOX,
  INVALID_PRIVATEKEY_OF_BOX,
  BOX_SUCCESSFULLY_UNLOCKED,
  BOX_NOT_LOCKED,
  INVALID_SERVER_RESPONSE
};



class UserMessage{
  public:
    static bool sendListMsg(int socketFD, const char username[64], const char password[64]); 
    static std::pair<ListMsgResponseStatus, std::unique_ptr<Message>> recvListMsgResponse(int socketFD);

    static bool sendAllocateMsg(int socketFD, const char username[64], const char password[64], BoxPublicID boxPublicID);
    static std::pair<AllocateMsgResponseStatus, std::unique_ptr<Message>> recvAllocateMsgResponse(int socketFD);

    static bool sendLockMsg(int socketFD, const char username[64], const char password[64], BoxPublicID boxPublicID);
    static std::pair<LockMsgResponseStatus, std::unique_ptr<Message>> recvLockMsgResponse(int socketFD);
 
    static bool sendUnlockMsg(int socketFD, const char username[64], const char password[64], BoxPublicID boxPublicID, BoxPrivateKey privateKey);
    static std::pair<UnlockMsgResponseStatus, std::unique_ptr<Message>> recvUnlockMsgResponse(int socketFD);
};





class ClientSocket{
  public:
    ClientSocket(ip_t ip, port_t port);
    ~ClientSocket() { close(socketFD); }

    int getSocketFD() { return socketFD; }

  private:
    int socketFD;

};

ClientSocket::ClientSocket(ip_t ip, port_t port){
  
  if((socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    throw std::invalid_argument("ClientSocket(): can not get socket");
 
  struct sockaddr_in servAddr;
  
  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(port);
  servAddr.sin_addr.s_addr = inet_addr(ip.c_str());
  
  if(connect(socketFD, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
    throw std::invalid_argument("ClientSocket(): can not connect to server");
}













std::pair<UnlockMsgResponseStatus, std::unique_ptr<Message>> UserMessage::recvUnlockMsgResponse(int socketFD)
{

  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(socketFD, &buffer[totalLen], 1u))
    return {UnlockMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};

  totalLen += 1u;  
  
  ServerMessageBase* msgBase = (ServerMessageBase *)buffer;

  switch(msgBase->type){
    case SMSG_INVALID_MSG:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {UnlockMsgResponseStatus::INVALID_USER_MSG, std::move(msg)};
    }

    case SMSG_INVALID_USERNAME:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {UnlockMsgResponseStatus::INVALID_USERNAME, std::move(msg)};
    } 

    case SMSG_INVALID_PASSWORD:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {UnlockMsgResponseStatus::INVALID_PASSWORD, std::move(msg)};
    }
    
    case SMSG_INVALID_PUBLIC_ID_OF_BOX:
    {
     
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {UnlockMsgResponseStatus::INVALID_PUBLIC_ID_OF_BOX, std::move(msg)};
    }

    case SMSG_BOX_PRIVATEKEY_IS_INVALID:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {UnlockMsgResponseStatus::INVALID_PRIVATEKEY_OF_BOX, std::move(msg)};
    }

    case SMSG_BOX_NOT_LOCKED:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {UnlockMsgResponseStatus::BOX_NOT_LOCKED, std::move(msg)};
    }


    case SMSG_BOX_IS_UNLOCKED:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {UnlockMsgResponseStatus::BOX_SUCCESSFULLY_UNLOCKED, std::move(msg)};
    }


    default:
    {
      return {UnlockMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
    }
  }
}








bool UserMessage::sendUnlockMsg(int socketFD, const char username[64], const char password[64], BoxPublicID boxPublicID, BoxPrivateKey privateKey)
{ 
  char buffer[sizeof(UserMessageUnlockBox)];
  
  UserMessageUnlockBox* umsg = (UserMessageUnlockBox *)buffer;
  umsg->usrMsgBase.type = UMSG_UNLOCK_BOX;

  memcpy(umsg->usrMsgBase.username, username, 64);
  memcpy(umsg->usrMsgBase.password, password, 64);
  memcpy(&umsg->publicID, &boxPublicID, sizeof(BoxPublicID));
  memcpy(&umsg->boxPrivateKey, &privateKey, sizeof(BoxPrivateKey));

  if(sendTo(socketFD, buffer, sizeof(UserMessageUnlockBox)))
    return true;
  else
    return false;
}







std::pair<LockMsgResponseStatus, std::unique_ptr<Message>> UserMessage::recvLockMsgResponse(int socketFD)
{ 
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(socketFD, &buffer[totalLen], 1u))
    return {LockMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};

  totalLen += 1u;  
  
  ServerMessageBase* msgBase = (ServerMessageBase *)buffer;

  switch(msgBase->type){
    case SMSG_INVALID_MSG:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {LockMsgResponseStatus::INVALID_USER_MSG, std::move(msg)};
    }

    case SMSG_INVALID_USERNAME:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {LockMsgResponseStatus::INVALID_USERNAME, std::move(msg)};
    } 

    case SMSG_INVALID_PASSWORD:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {LockMsgResponseStatus::INVALID_PASSWORD, std::move(msg)};
    }
    
    case SMSG_INVALID_PUBLIC_ID_OF_BOX:
    {
     
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {LockMsgResponseStatus::INVALID_PUBLIC_ID_OF_BOX, std::move(msg)};
    }

    case SMSG_BOX_ALREADY_RESERVED:
    { 

      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {LockMsgResponseStatus::BOX_ALREADY_RESERVED, std::move(msg)};
    }

    case SMSG_BOX_IS_NOT_ALLOCATED:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {LockMsgResponseStatus::BOX_NOT_ALLOCATED, std::move(msg)};
    }


    case SMSG_BOX_SUCCESSFULLY_LOCKED:
    {
      std::cout << "LOCKED" << std::endl;
      if(!recvFrom(socketFD, &buffer[totalLen], sizeof(ServerMessageBoxLocked) - sizeof(ServerMessageBase)))
        return {LockMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
     
      totalLen += sizeof(ServerMessageBoxLocked) - sizeof(ServerMessageBase);
 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {LockMsgResponseStatus::BOX_SUCCESSFULLY_LOCKED, std::move(msg)}; 
    }

    case SMSG_BOX_DOOR_IS_NOT_CLOSED:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {LockMsgResponseStatus::BOX_DOOR_IS_NOT_CLOSED, std::move(msg)};
    }

    default:
    {
      return {LockMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
    }
  }
}












bool UserMessage::sendLockMsg(int socketFD, const char username[64], const char password[64], BoxPublicID boxPublicID)
{ 
  char buffer[sizeof(UserMessageAllocateBox)];
  
  UserMessageLockBox* umsg = (UserMessageLockBox *)buffer;
  umsg->usrMsgBase.type = UMSG_LOCK_BOX;

  memcpy(umsg->usrMsgBase.username, username, 64);
  memcpy(umsg->usrMsgBase.password, password, 64);
  memcpy(&umsg->publicID, &boxPublicID, sizeof(BoxPublicID));

  if(sendTo(socketFD, buffer, sizeof(UserMessageLockBox)))
    return true;
  else
    return false;
}








std::pair<AllocateMsgResponseStatus, std::unique_ptr<Message>> UserMessage::recvAllocateMsgResponse(int socketFD){
  
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(socketFD, &buffer[totalLen], 1u))
    return {AllocateMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};

  totalLen += 1u;  
  
  ServerMessageBase* msgBase = (ServerMessageBase *)buffer;

  switch(msgBase->type){
    case SMSG_INVALID_MSG:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {AllocateMsgResponseStatus::INVALID_USER_MSG, std::move(msg)};
    }

    case SMSG_INVALID_USERNAME:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {AllocateMsgResponseStatus::INVALID_USERNAME, std::move(msg)};
    } 

    case SMSG_INVALID_PASSWORD:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {AllocateMsgResponseStatus::INVALID_PASSWORD, std::move(msg)};
    }

    case SMSG_BOX_SUCCESSFULLY_ALLOCATED:
    {
    
      if(!recvFrom(socketFD, &buffer[totalLen], sizeof(ServerMessageBoxAllocated) - sizeof(ServerMessageBase)))
        return {AllocateMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
     
      totalLen += sizeof(ServerMessageBoxAllocated) - sizeof(ServerMessageBase);
 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {AllocateMsgResponseStatus::BOX_SUCCESSFULLY_ALLOCATED, std::move(msg)}; 
    }

    case SMSG_INVALID_PUBLIC_ID_OF_BOX:
    {
     
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {AllocateMsgResponseStatus::INVALID_PUBLIC_ID_OF_BOX, std::move(msg)};
    }

    case SMSG_BOX_ALREADY_RESERVED:
    { 

      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {AllocateMsgResponseStatus::BOX_ALREADY_RESERVED, std::move(msg)};
    }

    default:
    {
      return {AllocateMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
    }
  }
}



bool UserMessage::sendAllocateMsg(int socketFD, const char username[64], const char password[64], BoxPublicID boxPublicID){
  
  char buffer[sizeof(UserMessageAllocateBox)];
  
  UserMessageAllocateBox* umsg = (UserMessageAllocateBox *)buffer;
  umsg->usrMsgBase.type = UMSG_ALLOCATE_BOX;

  memcpy(umsg->usrMsgBase.username, username, 64);
  memcpy(umsg->usrMsgBase.password, password, 64);
  memcpy(&umsg->publicID, &boxPublicID, sizeof(BoxPublicID));

  if(sendTo(socketFD, buffer, sizeof(UserMessageAllocateBox)))
    return true;
  else
    return false;
}


std::pair<ListMsgResponseStatus, std::unique_ptr<Message>> UserMessage::recvListMsgResponse(int socketFD){
  
  char buffer[256];
  std::size_t totalLen = 0u;

  if(!recvFrom(socketFD, &buffer[totalLen], 1u))
    return {ListMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};

  totalLen += 1u;  
  
  ServerMessageBase* msgBase = (ServerMessageBase *)buffer;

  switch(msgBase->type){
    case SMSG_INVALID_MSG:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {ListMsgResponseStatus::INVALID_USER_MSG, std::move(msg)};

    }

    case SMSG_INVALID_USERNAME:
    {
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {ListMsgResponseStatus::INVALID_USERNAME, std::move(msg)};
    } 

    case SMSG_INVALID_PASSWORD:
    { 
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;

      return {ListMsgResponseStatus::INVALID_PASSWORD, std::move(msg)};
    }

    case SMSG_BOXES_LIST:
    {
        //std::cout << "BOXES LIST" << std::endl;
        
        if(!recvFrom(socketFD, &buffer[totalLen], sizeof(ServerMessageFreeList::length)))
          return {ListMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};

        totalLen += sizeof(ServerMessageFreeList::length);
        
        ServerMessageFreeList* smsg = (ServerMessageFreeList *)buffer;

        //std::cout << "Messag len: " << (unsigned int)smsg->length << std::endl;
        //std::cout << "totalLen init: " << totalLen << std::endl;

        for(auto i = 0u ; i < (unsigned int)smsg->length ; ++i)
          if(recvFrom(socketFD, &buffer[totalLen], sizeof(BoxPublicID))){
            //std::cout << "PUBLIC ID: "; printAsChar(&buffer[totalLen], sizeof(BoxPublicID)); std::cout << std::endl;
            totalLen += sizeof(BoxPublicID);
          }
          else{
            //std::cout << "ERROR IN READING PUBLIC ID" << std::endl;
            return {ListMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
          }
        
      
        std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
        memcpy(msg.get()->buffer, buffer, totalLen);
        msg.get()->bufferLen = totalLen;
        //printAsChar((char *)msg.get()->buffer, msg.get()->bufferLen);
        return {ListMsgResponseStatus::BOXES_LIST, std::move(msg)};
    }

    case SMSG_BOX_IS_ALLOCATED:
    {
     // std::cout << "BOX IS ALLOCATED" << std::endl;
      
      if(!recvFrom(socketFD, &buffer[totalLen], sizeof(ServerMessageBoxIsAllocated::publicID)))
        return {ListMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
      
      totalLen += sizeof(ServerMessageBoxIsAllocated::publicID);

      //ServerMessageBoxIsAllocated* smsg = (ServerMessageBoxIsAllocated *)buffer;

     //std::cout << "BOX PUBLID ID: "; printAsChar(smsg->publicID.id, 8); std::cout << std::endl;


      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {ListMsgResponseStatus::BOX_IS_ALLOCATED, std::move(msg)};
    }

    case SMSG_BOX_IS_LOCKED:
    {
      //std::cout << "BOX IS LOCKED" << std::endl;

      if(!recvFrom(socketFD, &buffer[totalLen], sizeof(ServerMessageBoxIsLocked::publicID)))
        return {ListMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};

      totalLen += sizeof(ServerMessageBoxIsLocked::publicID);

      //ServerMessageBoxIsLocked* smsg = (ServerMessageBoxIsLocked *)buffer;

      //std::cout << "BOX PUBLID ID: "; printAsChar(smsg->publicID.id, 8); std::cout << std::endl;
      
      std::unique_ptr<Message> msg = std::make_unique<Message>(totalLen);
      memcpy(msg.get()->buffer, buffer, totalLen);
      msg.get()->bufferLen = totalLen;
      
      return {ListMsgResponseStatus::BOX_IS_LOCKED, std::move(msg)};
    }

    default:
    {
      return {ListMsgResponseStatus::INVALID_SERVER_RESPONSE, std::unique_ptr<Message>()};
    }

  }
}



bool UserMessage::sendListMsg(int socketFD, const char username[64], const char password[64]){

  char buffer[sizeof(UserMessageBase)];
  
  UserMessageBase* umsg = (UserMessageBase *)buffer;
  umsg->type = UMSG_LIST_FREE;

  memcpy(umsg->username, username, 64);
  memcpy(umsg->password, password, 64);
   
  if(sendTo(socketFD, buffer, sizeof(UserMessageBase)))
    return true;

  else
    return false;
}






void command_unlock(const char username[64], const char password[64], BoxPublicID boxPublicID, BoxPrivateKey privateKey){

  ClientSocket clientSock("127.0.0.1", SERVER_PORT);
  if(UserMessage::sendUnlockMsg(clientSock.getSocketFD(), username, password, boxPublicID, privateKey)){

    auto recvResult = UserMessage::recvUnlockMsgResponse(clientSock.getSocketFD());
  
    switch(recvResult.first){
      case UnlockMsgResponseStatus::INVALID_USER_MSG:
        std::cout << "INVALID USER MESSAGE" << std::endl;
        break;

      case UnlockMsgResponseStatus::INVALID_USERNAME:
        std::cout << "INVALID USERNAME" << std::endl;
        break;

      case UnlockMsgResponseStatus::INVALID_PASSWORD:
        std::cout << "INVALID PASSWORD" << std::endl;
        break;

      case UnlockMsgResponseStatus::INVALID_PUBLIC_ID_OF_BOX:
        std::cout << "INVALID PUBLIC ID OF BOX" << std::endl;
        break;

      case UnlockMsgResponseStatus::INVALID_SERVER_RESPONSE:
        std::cout << "INVALID SERVER RESPONSE" << std::endl;
        break;

      case UnlockMsgResponseStatus::BOX_NOT_LOCKED:
        std::cout << "BOX NOT LOCKED" << std::endl;
        break;

      case UnlockMsgResponseStatus::INVALID_PRIVATEKEY_OF_BOX:
        std::cout << "INVALID PRIVATEKEY OF BOX" << std::endl;
        break;

      case UnlockMsgResponseStatus::BOX_SUCCESSFULLY_UNLOCKED:
        std::cout << "BOX BOX_SUCCESSFULLY_UNLOCKED" << std::endl;
        break;
    }
  }
  else
    std::cout << "USER MESSAGE CAN NOT BE SENT" << std::endl;

}




void command_lock(const char username[64], const char password[64], BoxPublicID boxPublicID)
{
  ClientSocket clientSock("127.0.0.1", SERVER_PORT);
  if(UserMessage::sendLockMsg(clientSock.getSocketFD(), username, password, boxPublicID)){

    auto recvResult = UserMessage::recvLockMsgResponse(clientSock.getSocketFD());
  
    switch(recvResult.first){
      case LockMsgResponseStatus::INVALID_USER_MSG:
        std::cout << "INVALID USER MESSAGE" << std::endl;
        break;

      case LockMsgResponseStatus::INVALID_USERNAME:
        std::cout << "INVALID USERNAME" << std::endl;
        break;

      case LockMsgResponseStatus::INVALID_PASSWORD:
        std::cout << "INVALID PASSWORD" << std::endl;
        break;

      case LockMsgResponseStatus::INVALID_PUBLIC_ID_OF_BOX:
        std::cout << "INVALID PUBLIC ID OF BOX" << std::endl;
        break;

      case LockMsgResponseStatus::INVALID_SERVER_RESPONSE:
        std::cout << "INVALID SERVER RESPONSE" << std::endl;
        break;

      case LockMsgResponseStatus::BOX_ALREADY_RESERVED:
        std::cout << "BOX ALREADY RESERVED" << std::endl;
        break;

      case LockMsgResponseStatus::BOX_DOOR_IS_NOT_CLOSED:
        std::cout << "BOX DOOR IS NOT LOCKED" << std::endl;
        break;

      case LockMsgResponseStatus::BOX_NOT_ALLOCATED:
        std::cout << "BOX IS NOT ALLOCATED" << std::endl;
        break;

      case LockMsgResponseStatus::BOX_SUCCESSFULLY_LOCKED:
        std::cout << "BOX SUCCESSFULLY LOCKED" << std::endl;
        ServerMessageBoxLocked* smsg = (ServerMessageBoxLocked *)recvResult.second->buffer;
        std::cout << "ALLOCATION PERIOD: " << (int)smsg->allocationPeriod << std::endl;
        std::cout << "TIMESTAMP: " << (int)smsg->timestamp << std::endl;
        std::cout << "PRIVATE KEY: " << smsg->privateKey.privateKey << std::endl;
        break;

    }
  }
  else
    std::cout << "USER MESSAGE CAN NOT BE SENT" << std::endl;

}

void command_allocate(const char username[64], const char password[64], BoxPublicID boxPublicID){

  ClientSocket clientSock("127.0.0.1", SERVER_PORT);
  if(UserMessage::sendAllocateMsg(clientSock.getSocketFD(), username, password, boxPublicID)){

    auto recvResult = UserMessage::recvAllocateMsgResponse(clientSock.getSocketFD());
  
    switch(recvResult.first){
      case AllocateMsgResponseStatus::INVALID_USER_MSG:
        std::cout << "INVALID USER MESSAGE" << std::endl;
        break;

      case AllocateMsgResponseStatus::INVALID_USERNAME:
        std::cout << "INVALID USERNAME" << std::endl;
        break;

      case AllocateMsgResponseStatus::INVALID_PASSWORD:
        std::cout << "INVALID PASSWORD" << std::endl;
        break;

      case AllocateMsgResponseStatus::INVALID_PUBLIC_ID_OF_BOX:
        std::cout << "INVALID PUBLIC ID OF BOX" << std::endl;
        break;

      case AllocateMsgResponseStatus::INVALID_SERVER_RESPONSE:
        std::cout << "INVALID SERVER RESPONSE" << std::endl;
        break;

      case AllocateMsgResponseStatus::BOX_ALREADY_RESERVED:
        std::cout << "BOX ALREADY RESERVED" << std::endl;
        break;

      case AllocateMsgResponseStatus::BOX_SUCCESSFULLY_ALLOCATED:
        std::cout << "BOX SUCCESSFULLY ALLOCATED" << std::endl;
        ServerMessageBoxAllocated* smsg = (ServerMessageBoxAllocated *)recvResult.second->buffer;
        std::cout << "ALLOCATION PERIOD: " << (int)smsg->allocationPeriod << std::endl;
        std::cout << "TIMESTAMP: " << (int)smsg->timestamp << std::endl;
        break;
    }
  }
  else
    std::cout << "USER MESSAGE CAN NOT BE SENT" << std::endl;

}



void command_list(const char username[64], const char password[64]){

  ClientSocket clientSock("127.0.0.1", SERVER_PORT);


  if(UserMessage::sendListMsg(clientSock.getSocketFD(), username, password)){
    auto recvResult = UserMessage::recvListMsgResponse(clientSock.getSocketFD());
  
    switch(recvResult.first){
      case ListMsgResponseStatus::INVALID_USER_MSG:
        std::cout << "INVALID USER MESSAGE" << std::endl;
        break;

      case ListMsgResponseStatus::INVALID_USERNAME:
        std::cout << "INVALID USERNAME" << std::endl;
        break;

      case ListMsgResponseStatus::INVALID_PASSWORD:
        std::cout << "INVALID PASSWORD" << std::endl;
        break;

      case ListMsgResponseStatus::BOXES_LIST:
        {
          std::cout << "BOXES LIST" << std::endl;
          
          ServerMessageFreeList* smsg = (ServerMessageFreeList *)recvResult.second->buffer;
          //printAsChar((char *)recvResult.second->buffer, recvResult.second->bufferLen);
          BoxPublicID* boxesList = (BoxPublicID *)(((char *)(recvResult.second->buffer)) + sizeof(ServerMessageFreeList::srvMsgBase) + sizeof(ServerMessageFreeList::length));

          for(auto i = 0u; i < smsg->length ; ++i){
            std::cout << "BOX " << (int)i << ": "; printAsChar(boxesList[i].id, 8u); std::cout << std::endl;
          } 
        }
        break;

      case ListMsgResponseStatus::BOX_IS_ALLOCATED:
        {
            std::cout << "USER HAVE ALLOCATED BOX: ";
            ServerMessageBoxIsAllocated* smsg = (ServerMessageBoxIsAllocated *)recvResult.second->buffer;
            printAsChar(smsg->publicID.id, sizeof(smsg->publicID.id));
            std::cout << std::endl;
        }
        break;

      case ListMsgResponseStatus::BOX_IS_LOCKED:
        {
          std::cout << "USER HAVE LOCKED BOX: ";
          ServerMessageBoxIsLocked* smsg = (ServerMessageBoxIsLocked *)recvResult.second->buffer;
          printAsChar(smsg->publicID.id, sizeof(smsg->publicID.id));
          std::cout << std::endl;
        }
        break;

      case ListMsgResponseStatus::INVALID_SERVER_RESPONSE:
        std::cout << "INVALID SERVER RESPONSE" << std::endl;
        break;
    } 
  }
  else
    std::cout << "USER MESSAGE CAN NOT BE SENT" << std::endl;
}




int main(int argc, char *argv[]){
  
  std::string command;
  char username[64] = "mirsad.ostrakovic@fet.ba";
  char password[64] = "mirso123";
  char id[] = "abcd1234";
  BoxPublicID boxPublicID;
  BoxPrivateKey boxPrivateKey;
  memcpy(boxPublicID.id, id, sizeof(BoxPublicID::id));

  std::cout << ">>";
  while(std::getline(std::cin, command)){
    if(command == "LIST")
      command_list(username, password);
    else if(command == "ALLOCATE")
      command_allocate(username, password, boxPublicID);
    else if(command == "LOCK")
      command_lock(username, password, boxPublicID);
    else if(command == "UNLOCK")
      command_unlock(username, password, boxPublicID, boxPrivateKey);
    else
      std::cout << "INVALID COMMAND" << std::endl;
    
    std::cout << ">>";
  }
  
  return 0;
}
