#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

bool recvFrom(int socketFD, void* buffer, std::size_t msgLength){

  std::size_t readLen = 0u;
  std::size_t currentLen;

  while(true){ 
    
    if((currentLen = recv(socketFD, &((char *)(buffer))[readLen], msgLength - readLen, 0)) <= 0)
      return false;

    //std::cout << "before: readLen: " << readLen << " currentLen: " << currentLen << std::endl;
    readLen += currentLen;
   
    //std::cout << "after: readLen: " << readLen << " currentLen: " << currentLen << std::endl;

    //std::cout << "msgLength: " << msgLength << std::endl;
    if( readLen == msgLength )
      return true;
  }
}


bool sendTo(int socketFD, void* buffer, std::size_t msgLength){

  std::size_t sendLen = 0u;
  std::size_t currentLen;

  while(true){ 
    
    if((currentLen = send(socketFD, &((char *)(buffer))[sendLen], msgLength - sendLen, 0)) <= 0)
      return false;
    
    //std::cout << "before: sendLen: " << sendLen << " currentLen: " << currentLen << " msgLength: " << msgLength << std::endl;
    sendLen += currentLen;
    //std::cout << "after: sendLen: " << sendLen << " currentLen: " << currentLen << " msgLength: " << msgLength << std::endl;

    if( sendLen == msgLength )
      return true;
  }
}



class SocketGuard{
  public:
    SocketGuard(int socket) : socketFD{ socket } {}
    ~SocketGuard() { std::cerr << "Close socket: " << socketFD << std::endl; close(socketFD); }

  private:
    int socketFD;
};


#endif
