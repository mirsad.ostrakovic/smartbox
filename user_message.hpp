#ifndef USER_MESSAGE_HPP
#define USER_MESSAGE_HPP

struct BoxPublicID{
  char id[8];
};

struct BoxPrivateKey{
  char privateKey[128];
};

#define UMSG_LIST_FREE 0x1
#define UMSG_ALLOCATE_BOX 0x2
#define UMSG_LOCK_BOX 0x3
#define UMSG_PING 0x4
#define UMSG_UNLOCK_BOX 0x5

struct UserMessageBase{
  char type;
  char username[64];
  char password[64];
};

struct UserMessageAllocateBox{
  UserMessageBase usrMsgBase;
  BoxPublicID publicID;
};

struct UserMessageLockBox{ 
  UserMessageBase usrMsgBase;
  BoxPublicID publicID;
};

struct UserMessageUnlockBox{
  UserMessageBase usrMsgBase;
  BoxPublicID publicID;
  BoxPrivateKey boxPrivateKey;
};

// BASE PART OF EVERY MESSAGE
#define SMSG_INVALID_MSG 0x1
#define SMSG_INVALID_USERNAME 0x2
#define SMSG_INVALID_PASSWORD 0x3
// FOR LIST BOX
#define SMSG_BOXES_LIST 0x4
#define SMSG_BOX_IS_ALLOCATED 0x5
#define SMSG_BOX_IS_LOCKED 0x6
// FOR ALLOCATE BOX, LOCK BOX and UNLOCK BOX
#define SMSG_INVALID_PUBLIC_ID_OF_BOX 0x7
// FOR ALLOCATE BOX AND LOCK BOX
#define SMSG_BOX_ALREADY_RESERVED 0x8
// FOR ALLOCATE BOX
#define SMSG_BOX_SUCCESSFULLY_ALLOCATED 0x9
// FOR LOCK BOX
#define SMSG_BOX_IS_NOT_ALLOCATED 0xA
#define SMSG_BOX_SUCCESSFULLY_LOCKED 0xB
#define SMSG_BOX_DOOR_IS_NOT_CLOSED 0xC
// FOR UNLOCK BOX
#define SMSG_BOX_IS_UNLOCKED 0xD
#define SMSG_BOX_PRIVATEKEY_IS_INVALID 0xE
#define SMSG_BOX_NOT_LOCKED 0xF


// BASE PART OF EVERY MESSAGE
struct ServerMessageBase{
  char type; // <= SMSG_INVALID_MSG || SMSG_INVALID_USERNAME || SMSG_INVALID_PASSWORD
};

// FOR LIST BOX
struct ServerMessageFreeList{
  ServerMessageBase srvMsgBase; // <= SMSG_BOXES_LIST
  unsigned char length;
  BoxPublicID* boxList;
};

struct ServerMessageBoxIsAllocated{
  ServerMessageBase srvMsgBase; // <= SMGS_BOX_IS_ALLOCATED
  BoxPublicID publicID;
};

struct ServerMessageBoxIsLocked{
  ServerMessageBase srvMsgBase; // <= SMG_BOX_IS_LOCKED
  BoxPublicID publicID;
};

//FOR ALLOCATE BOX
struct ServerMessageBoxAllocated{
  ServerMessageBase srvMsgBase;
  unsigned char allocationPeriod;
  unsigned int timestamp;
};

//FOR LOCK BOX
struct ServerMessageBoxLocked{
  ServerMessageBase srvMsgBase;
  unsigned char allocationPeriod;
  unsigned int timestamp;
  BoxPrivateKey privateKey;
};


#endif



